from setuptools import setup

setup(name='tnetserver',
	version='1.1',
	description='Tempgard gateway server',
	url="http://tempnetz.com",
	author='Vaughn Coetzee',
	author_email='ugobyte@gmail.com',
	packages=['tggateway'],
	zip_safe=True)

