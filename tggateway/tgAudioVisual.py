#!/usr/bin/env python2

''' @file : tgAudioVisual.py
	@brief : Audio and visual.
'''

import os
import sys
import copy
import time
import signal
import json
import logging
import threading
from twisted.internet import reactor

import tggateway.tgEvent as tgEvent
from tggateway.tgModel import Model

AUDIO_VISUAL_CONFIG_FILE 	= '/home/tgard/config/audiovisual.json'

#AUDIO_CTRL_MUTE			= 'gpio9_pe8'
#AUDIO_CTRL_BUZZER   		= 'gpio30_pb5'
AUDIO_CTRL_A1				= 'gpio31_pb6'
AUDIO_CTRL_A2				= 'gpio32_pb7'
AUDIO_CTRL_BUZZER 			= 'gpio30_pb5'
#AUDIO_CTRL_BUZZER_A2		= 'gpio11_pe10' -> actually for touchscreen beep

GPIO_EXPORT_FILE 			= '/sys/class/gpio/export'
GPIO_DIRECTION_FILE 		= '/sys/class/gpio/{0}/direction'
GPIO_VALUE_FILE 			= '/sys/class/gpio/{0}/value'

GPIO_DIRECTION_OUT			= 'out'
GPIO_DIRECTION_IN			= 'in'

GPIO_VALUE_OFF				= '0'
GPIO_VALUE_ON				= '1'

AUDIO_ALARM_STATE_OFF		= 0
AUDIO_ALARM_STATE_A1		= 1
AUDIO_ALARM_STATE_A2		= 2

AV_DISABLED					= 0
AV_ENABLED 					= 1

HRD = ['Alarms off', 'Alarm 1 on', 'Alarm 2 on']

class AudioVisual(Model):

	def __init__(self, eventMgr=None):
		''' @fn : __init__
			@brief : Class initialisation.
		'''
		super(AudioVisual, self).__init__('AudioVisual')
		self.config = {}
		self.muted = {"now": False, "next": False}
		self.audAlarmState = {"now": AUDIO_ALARM_STATE_OFF, "next": AUDIO_ALARM_STATE_OFF }
		self.muteCount = 0
		self.mutedAtTime = 0
		logging.debug('AudioVisual: Initialised.')

	def getLiveState(self):
		''' @brief : Get live status. '''

		try:
			if self.muted["now"]:
				muteTimeRemaining = self.config['AudioMuteTimeout']*60 - (int(time.time()) - self.mutedAtTime)
				status = '{},{},{},{}'.format(1, muteTimeRemaining, self.muteCount, self.config['AudioMute'])
			else:
				status = '{},{},{},{}'.format(0, 0, self.muteCount, self.config['AudioMute'])
		except Exception as e:
			logging.debug('AudioVisual: Get live status error = {}'.format(e))
			status = '{},{},{},{}'.format(0, 0, self.muteCount, self.config['AudioMute'])

		return status

	def setDefaultConfig(self):
		self.config = {}
		self.config["Visual"] = True
		self.config["Audio"] = True
		self.config["Buzzer"] = True
		self.config["AudioMute"] = 5
		self.config["AudioMuteTimeout"] = 20

	def loadConfig(self):
		'''
			@brief : Load config from file.
		'''
		try:
			with open(AUDIO_VISUAL_CONFIG_FILE, 'r') as f:
				self.config = json.load(f)

			someAttributesNotFound = False
			if "Visual" not in self.config:
				self.config["Visual"] = True
				someAttributesNotFound = True

			if "Audio" not in self.config:
				self.config["Audio"] = True
				someAttributesNotFound = True

			if "Buzzer" not in self.config:
				self.config["Buzzer"] = True
				someAttributesNotFound = True

			if "AudioMute" not in self.config:
				self.config["AudioMute"] = 5
				someAttributesNotFound = True

			if "AudioMuteTimeout" not in self.config:
				self.config["AudioMuteTimeout"] = 20
				someAttributesNotFound = True

			if someAttributesNotFound:
				self.dumpConfig()

			logging.info('AudioVisual: Config loaded from file.')

		except Exception as e:
			logging.error("AudioVisual: Load config error {}.".format(e))
			self.setDefaultConfig()

		logging.debug('AudioVisual: Visual enabled {0}.'.format(self.config['Visual']))
		logging.debug('AudioVisual: Audio buzzer enabled {0}.'.format(self.config['Buzzer']))
		logging.debug('AudioVisual: Audio enabled {0}.'.format(self.config['Audio']))
		logging.debug('AudioVisual: Audio mute {0}.'.format(self.config['AudioMute']))
		logging.debug('AudioVisual: Audio mute timeout {0}.'.format(self.config['AudioMuteTimeout']))
	
		self.setup()
		self.configured = True

	def dumpConfig(self):
		'''
			@brief : Dump config to file.
		'''
		try:
			with open(AUDIO_VISUAL_CONFIG_FILE, 'w') as f:
				json.dump(self.config, f)
		
			logging.info('AudioVisual: Config dumped to file.')
			return True
		except Exception as e:
			logging.error("AudioVisual: Dump config error {0}.".format(e))
			return False

	def setConfig(self, config):
		''' 
			@brief : Set config.
		'''
		audMute = self.config['AudioMute']
		backup = copy.deepcopy(self.config)
		self.config.clear()
		try:	
			self.config	= config
			
			# validate 
			if "Visual" not in self.config or "Audio" not in self.config or "AudioMute" not in self.config or "AudioMuteTimeout" not in self.config:
				logging.warning("AudioVisual: Config not valid")
				self.config = copy.deepcopy(backup)
				return False

			if self.config["Visual"] != False and self.config["Visual"] != True:
				logging.warning("AudioVisual: Visual setting not valid")
				self.config = copy.deepcopy(backup)
				return False

			if self.config["Audio"] != False and self.config["Audio"] != True:
				logging.warning("AudioVisual: Audio setting not valid")
				self.config = copy.deepcopy(backup)
				return False

			if self.config["AudioMute"] < 0 or self.config["AudioMute"] > 9:
				logging.warning("AudioVisual: AudioMute setting not valid")
				self.config = copy.deepcopy(backup)
				return False

			if self.config["AudioMuteTimeout"] < 1 or self.config["AudioMuteTimeout"] > 1440:
				logging.warning("AudioVisual: AudioMuteTimeout setting not valid")
				self.config = copy.deepcopy(backup)
				return False
			
			if not self.dumpConfig():
				self.config = copy.deepcopy(backup)
				return False

			logging.debug('AudioVisual: Config set.')

			# reset mute count if audio mute changed
			if audMute != self.config['AudioMute']:
				logging.info('AudioVisual: AudioMute changed, reseting muteCount to 0.')
				self.muteCount = 0

			self.configured = True

			return True
		except Exception as e:
			logging.error('AudioVisual: Config set error {0}.'.format(e))

			# restore previous
			self.config = copy.deepcopy(backup)
			return False

	def getConfig(self):
		''' 
			@brief : Get config.
		'''
		return copy.deepcopy(self.config)

	def gpioExists(self, gpio):
		'''
			@brief : Check gpio file exists.
		'''
		fDirection = GPIO_DIRECTION_FILE.format(gpio)
		if os.path.isfile(fDirection) and os.access(fDirection, os.R_OK):
			return True
		else:
			return False

	def exportGpio(self, gpio):
		'''
			@brief : Export gpio.
		'''
		
		if not self.gpioExists(gpio):
			with open(GPIO_EXPORT_FILE, 'w') as f:
				f.write(gpio)

	def directionGpio(self, gpio, direction):
		'''
			@brief : Set direction.
		'''
		fDirection = GPIO_DIRECTION_FILE.format(gpio) 
		with open(fDirection, 'w') as f:
			f.write(direction)
		
	def valueGpio(self, gpio, value):
		'''
			@brief : Set value.
		'''
		fValue = GPIO_VALUE_FILE.format(gpio) 
		with open(fValue, 'w') as f:
			f.write(value)

	def setup(self):
		'''
			@brief : Export gpio.
		'''
		self.exportGpio(AUDIO_CTRL_A1)
		self.exportGpio(AUDIO_CTRL_A2)
		self.exportGpio(AUDIO_CTRL_BUZZER)

		self.directionGpio(AUDIO_CTRL_A1, GPIO_DIRECTION_OUT)
		self.directionGpio(AUDIO_CTRL_A2, GPIO_DIRECTION_OUT)
		self.directionGpio(AUDIO_CTRL_BUZZER, GPIO_DIRECTION_OUT)

		self.valueGpio(AUDIO_CTRL_A1, GPIO_VALUE_OFF)
		self.valueGpio(AUDIO_CTRL_A2, GPIO_VALUE_OFF)
		self.valueGpio(AUDIO_CTRL_BUZZER, GPIO_VALUE_OFF)


	def buzzerOn(self):
		'''@brief : Buzzer on.'''
		if self.config['Buzzer']:
			self.valueGpio(AUDIO_CTRL_BUZZER, GPIO_VALUE_ON)

	def buzzerOff(self):
		'''@brief : Buzzer off.'''
		self.valueGpio(AUDIO_CTRL_BUZZER, GPIO_VALUE_OFF)
	
	def a1On(self):
		'''@brief : A1 on.'''
		self.valueGpio(AUDIO_CTRL_A1, GPIO_VALUE_ON)

	def a1Off(self):
		'''@brief : A1 off.'''
		self.valueGpio(AUDIO_CTRL_A1, GPIO_VALUE_OFF)

	def a2On(self):
		'''@brief : A2 on.'''
		self.valueGpio(AUDIO_CTRL_A2, GPIO_VALUE_ON)

	def a2Off(self):
		'''@brief : A2 off.'''
		self.valueGpio(AUDIO_CTRL_A2, GPIO_VALUE_OFF)

	def allOff(self):
		''' @brief : Turn all off '''
		self.a1Off()
		self.a2Off()
		self.buzzerOff()

	def audioCtrl(self, evtopic):
		'''@brief : Call from shell ''' 
		if evtopic == tgEvent.EVTOPIC_AUV_A1_ON:
			self.a1On()
		
		elif evtopic == tgEvent.EVTOPIC_AUV_A1_OFF:
			self.a1Off()

		elif evtopic == tgEvent.EVTOPIC_AUV_A2_ON:
			self.a2On()	

		elif evtopic == tgEvent.EVTOPIC_AUV_A2_OFF:
			self.a2Off()

		elif evtopic == tgEvent.EVTOPIC_AUV_BUZZ_ON:
			self.buzzerOn()

		elif evtopic == tgEvent.EVTOPIC_AUV_BUZZ_OFF:
			self.buzzerOff()

		elif evtopic == tgEvent.EVTOPIC_AUV_OFF:
			self.allOff()	

	def stop(self):
		''' @brief : Stop, called from shell. '''
		logging.info('AudioVisual: Stopping.')
		super(AudioVisual,self).stop()
		self.audAlarmState = AUDIO_ALARM_STATE_OFF
		self.allOff()
		

	def alert(self, evtopic):
		'''@brief : Alert '''

		# set the state to transition to
		if evtopic == tgEvent.EVTOPIC_AUV_STATE_A0:
			logging.info("AudioVisual: Alert transition to no alarm")
			self.audAlarmState["next"] = AUDIO_ALARM_STATE_OFF

		elif evtopic == tgEvent.EVTOPIC_AUV_STATE_A1:
			logging.info("AudioVisual: Alert transition to a1")
			self.audAlarmState["next"] = AUDIO_ALARM_STATE_A1

		elif evtopic == tgEvent.EVTOPIC_AUV_STATE_A2:
			logging.info("AudioVisual: Alert transition to a2")
			self.audAlarmState["next"] = AUDIO_ALARM_STATE_A2

		elif evtopic == tgEvent.EVTOPIC_AUV_MUTE:
			# set next mute state to opposite of the current state
			logging.info("AudioVisual: Mute toggled")
			self.muted["next"] = not self.muted["now"]
		else:
			logging.warning("AudioVisual: Unknown state in evtopic")
				
	def run(self):

		mark = False
		# used to detect state change of audio alert so gpio can be set low if changed to disabled while gpio in mark state
		audioAlertState = self.config["Audio"]
		while True:

			time.sleep(0.5)

			# break if stop thread
			if self.stopThread:
				break

			# audio alert state change
			if audioAlertState != self.config["Audio"]:
				logging.info("AudioVisual: Audio alert changed from {} to {}".format(audioAlertState, self.config["Audio"]))
				# if changed from enabled to disabled, turn alarms off
				if audioAlertState and not self.config["Audio"]:
					self.allOff()

				audioAlertState = self.config["Audio"]

			# ignore if audio disabled
			if not self.config["Audio"]:
				time.sleep(5)
				continue

			# mute state change
			if self.muted["next"] != self.muted["now"]:
				logging.info("AudioVisual: Mute state changed from {} to {}".format(self.muted["now"], self.muted["next"]))

				# unmuted to muted, decrease the count
				if self.muted["next"]:
					
					if self.muteCount >= self.config['AudioMute']:
						logging.warning("AudioVisual: Audio mute limit of {} reached, not muting".format(self.config['AudioMute'] ))
						self.muted["now"] = self.muted["next"] = False
					else:
						self.muteCount += 1
						self.muted["now"] = self.muted["next"]
						self.allOff()
						self.mutedAtTime = time.time()
				else:
					self.muted["now"] = self.muted["next"]

			# check if mute timeout expired if already muted
			if self.muted["now"]:
				if time.time() - self.mutedAtTime < (self.config["AudioMuteTimeout"]*60):
					time.sleep(5)
					continue

				logging.info("AudioVisual: Mute time complete")
				self.muted["now"] = self.muted["next"] = False

			# state change for audio
			if self.audAlarmState["next"] != self.audAlarmState["now"]:
				logging.info("AudioVisual: Alarm state changed from {} to {}".format(self.audAlarmState["now"], self.audAlarmState["next"]))
				
				# change from alarm state to none
				if self.audAlarmState["next"] == AUDIO_ALARM_STATE_OFF:
					self.allOff()

				# set the new state
				self.audAlarmState["now"] = self.audAlarmState["next"]

			# a2 alarm state
			if self.audAlarmState["now"] == AUDIO_ALARM_STATE_A2:
				if mark:
					self.buzzerOn()
					#self.a2On()
					mark = False
				else:
					self.buzzerOff()
					#self.a2Off()
					mark = True
					time.sleep(1)

			# a1 alarm state
			elif self.audAlarmState["now"] == AUDIO_ALARM_STATE_A1:
				if mark:
					self.buzzerOn()
					#self.a1On()
					mark = False
				else:
					self.buzzerOff()
					#self.a1Off()
					mark = True
					time.sleep(3)

		


def fSignalHandler(signal, frame):		
	''' @fn fSignalHandler
		@brief : Signal handler.
	'''
	tgAv.stop()
	logging.info('AudioVisual: Exiting application.')
	sys.exit()

if __name__ == '__main__':
	
	signal.signal( signal.SIGINT, fSignalHandler )
	signal.signal( signal.SIGTERM, fSignalHandler )

	argc = len(sys.argv)	

	# root logger
	logger = logging.getLogger('')
	logger.setLevel(logging.DEBUG)
	
	# format for logging
	format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

	# add stdout stream handler
	stdouth = logging.StreamHandler(sys.stdout)
	stdouth.setFormatter(format)
	logger.addHandler(stdouth)

	tgAv = AudioVisual()
	tgAv.loadConfig()

	if sys.argv[1] == '--setup':
		tgAv.setup()

	elif sys.argv[1] == '--ctrl':
		if sys.argv[2] == 'buzz':
			tgAv.buzzerOn()
			time.sleep(5)
			tgAv.buzzerOff()
		elif sys.argv[2] == 'a1':
			tgAv.a1On()
			time.sleep(5)
			tgAv.a1Off()
		elif sys.argv[2] == 'a2':
			tgAv.a2On()
			time.sleep(5)
			tgAv.a1On()

	elif sys.argv[1] == '--alert':
		tgAv.config['AudioMuteTimeout'] = 1
		tgAv.alert(tgEvent.EVTOPIC_AUV_STATE_A0)
		time.sleep(10)
		tgAv.alert(tgEvent.EVTOPIC_AUV_STATE_A1)
		time.sleep(10)
		tgAv.alert(tgEvent.EVTOPIC_AUV_STATE_A2)
		time.sleep(10)
		tgAv.alert(tgEvent.EVTOPIC_AUV_MUTE)
		time.sleep(10)
		tgAv.alert(tgEvent.EVTOPIC_AUV_STATE_A1)
		time.sleep(10)
		tgAv.alert(tgEvent.EVTOPIC_AUV_STATE_A0)

