#!/usr/bin/python

import gobject

import dbus
import dbus.service
import dbus.mainloop.glib
import sys
import os
import json

class Canceled(dbus.DBusException):
	_dbus_error_name = "net.connman.Error.Canceled"

class LaunchBrowser(dbus.DBusException):
	_dbus_error_name = "net.connman.Agent.Error.LaunchBrowser"

class Agent(dbus.service.Object):
	name = None
	ssid = None
	identity = None
	passphrase = None
	wpspin = None
	username = None
	password = None

	@dbus.service.method("net.connman.Agent",
					in_signature='', out_signature='')
	def Release(self):
		print("Release")
		mainloop.quit()

	def input_passphrase(self):
		response = {}

		if not self.identity and not self.passphrase and not self.wpspin:
			print "Service credentials requested, type cancel to cancel"
			args = raw_input('Answer: ')

			for arg in args.split():
				if arg.startswith("cancel"):
					response["Error"] = arg
				if arg.startswith("Identity="):
					identity = arg.replace("Identity=", "", 1)
					response["Identity"] = identity
				if arg.startswith("Passphrase="):
					passphrase = arg.replace("Passphrase=", "", 1)
					response["Passphrase"] = passphrase
				if arg.startswith("WPS="):
					wpspin = arg.replace("WPS=", "", 1)
					response["WPS"] = wpspin
					break
		else:
			if self.identity:
				print 'Requesting identity = {}'.format(self.identity)
				response["Identity"] = self.identity
			if self.passphrase:
				print 'Requesting passphrase = {}'.format(self.passphrase)
				response["Passphrase"] = self.passphrase
			if self.wpspin:
				response["WPS"] = self.wpspin

		return response

	def input_username(self):
		response = {}

		if not self.username and not self.password:
			print "User login requested, type cancel to cancel"
			print "or browser to login through the browser by yourself."
			args = raw_input('Answer: ')

			for arg in args.split():
				if arg.startswith("cancel") or arg.startswith("browser"):
					response["Error"] = arg
				if arg.startswith("Username="):
					username = arg.replace("Username=", "", 1)
					response["Username"] = username
				if arg.startswith("Password="):
					password = arg.replace("Password=", "", 1)
					response["Password"] = password
		else:
			if self.username:
				response["Username"] = self.username
			if self.password:
				response["Password"] = self.password

		return response

	def input_hidden(self):
		response = {}

		if not self.name and not self.ssid:
			args = raw_input('Answer ')

			for arg in args.split():
				if arg.startswith("Name="):
					name = arg.replace("Name=", "", 1)
					response["Name"] = name
					break
				if arg.startswith("SSID="):
					ssid = arg.replace("SSID", "", 1)
					response["SSID"] = ssid
					break
		else:
			if self.name:
				response["Name"] = self.name
			if self.ssid:
				response["SSID"] = self.ssid

		return response

	@dbus.service.method("net.connman.Agent",
					in_signature='oa{sv}',
					out_signature='a{sv}')
	def RequestInput(self, path, fields):
		print "RequestInput (%s,%s)" % (path, fields)

		response = {}

		if fields.has_key("Name"):
			response.update(self.input_hidden())
		if fields.has_key("Passphrase"):
			response.update(self.input_passphrase())
		if fields.has_key("Username"):
			response.update(self.input_username())

		if response.has_key("Error"):
			if response["Error"] == "cancel":
				raise Canceled("canceled")
				return
			if response["Error"] == "browser":
				raise LaunchBrowser("launch browser")
				return

		print "returning (%s)" % (response)

		return response

	@dbus.service.method("net.connman.Agent",
					in_signature='os',
					out_signature='')
	def RequestBrowser(self, path, url):
		print "RequestBrowser (%s,%s)" % (path, url)

		print "Please login through the given url in a browser"
		print "Then press enter to accept or some text to cancel"

		args = raw_input('> ')

		if len(args) > 0:
			raise Canceled("canceled")

		return

	@dbus.service.method("net.connman.Agent",
					in_signature='os',
					out_signature='')
	def ReportError(self, path, error):
		print "ReportError %s, %s" % (path, error)
		retry = raw_input("Retry service (yes/no): ")
		if (retry == "yes"):
			class Retry(dbus.DBusException):
				_dbus_error_name = "net.connman.Agent.Error.Retry"

			raise Retry("retry service")
		else:
			return


	@dbus.service.method("net.connman.Agent",
					in_signature='', out_signature='')
	def Cancel(self):
		print "Cancel"

def getIpAddress(iface):
	''' @fn getIpAddress
	    @brief : Get active interface IP address
	'''

	ipaddr = ''
	for i in range(3):
		
		ifname = "{}{}".format(iface,i)
		try:
			osout = os.popen('ifconfig {} | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1'.format(ifname))
			#logging.debug(osout)
			ipaddr = osout.read()
			ip = ''

			if ipaddr[:8] == '192.168.':
				ip = ipaddr.strip()

			elif ipaddr[:3] == '10.':
				ip = ipaddr.strip()
				
			elif ipaddr[:4] == '172.':
				ip = ipaddr.strip()

			print 'Network: IP address = {}.'.format(ip)
			return ip
		except Exception as e:
			print 'Network: Get ip addr error {}.'.format(e)       
			return ''

if __name__ == '__main__':

	dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
	bus = dbus.SystemBus()
	manager = dbus.Interface(bus.get_object('net.connman', "/"),'net.connman.Manager')
	agentpath = "/test/agent"
	object = Agent(bus, agentpath)


	# ssid and passphrase in config
	try:
		with open('/home/tgard/config/network.json', 'r') as f:
			config = json.load(f)

	except Exception as e:
		print 'Unable to load config.'
		sys.exit(1)	


	if 'ssid' not in config:
		print 'TgNetwork: No ssid in config'
		sys.exit(2)

	if 'psk' not in config:
		print 'TgNetwork: No psk in config.'
		sys.exit(3)

	servicePath = ''
	try:
		services = manager.GetServices()

		for service in services:
			(path,params) = service
			if 'Name' in params:
				if params['Name'] == config['ssid']:
					servicePath = path

	except dbus.exceptions.DBusException:
		print 'Dbus exception.'
		sys.exit(4)

	if servicePath == '':
		print 'No service for ssid {}'.format(config['ssid'])
		sys.exit(5)

	print 'Service path {}'.format(servicePath)

	tokens = servicePath.split('/')
	# register agent	
	if len(tokens) != 5 or 'wifi' not in tokens[4]:
		print 'No valid identity.'
		sys.exit(6)

	print 'Identity {}'.format(tokens[4])
	print 'Psk {}'.format(config['psk'])
	object.identity = tokens[4]
	object.passphrase = config['psk'] 

	try:
		manager.RegisterAgent(agentpath)
	except:
		print "Cannot register connman agent."
		sys.exit(7)

	try:
		service = dbus.Interface(bus.get_object("net.connman", servicePath), "net.connman.Service")
		service.Connect()
	except Exception as e:
		print e

	mainloop = gobject.MainLoop()
	mainloop.run()

	#manager.UnregisterAgent(path)