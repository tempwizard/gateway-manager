#!/usr/bin/env python

''' @file : tgNetwork.py
	@brief : Network manager.
'''

import logging
import time
import sys
import os
import subprocess
import json

import ConfigParser
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject

from tggateway.tgModel import Model
import tggateway.tgEvent as tgEvent

INTERFACE_UNKNOWN 	= 0
INTERFACE_ETHERNET 	= 1
INTERFACE_WIFI		= 2
INTERFACE_MOBILE 	= 3

hrInterface = ['None', 'Ethernet', 'Wifi', 'Mobile']

TECHNOLOGY_ETH = '/net/connman/technology/ethernet'
TECHNOLOGY_WIFI = '/net/connman/technology/wifi'
TECHNOLOGY_CELL = '/net/connman/technology/cellular'



class Agent(dbus.service.Object):
	'''	@brief  : Dbus object wrapped by connman simple aganet. '''

	name = None
	ssid = None
	identity = None
	passphrase = None
	wpspin = None
	username = None
	password = None

	@dbus.service.method("net.connman.Agent",
		in_signature='', out_signature='')
	def Release(self):
		loop.quit()

	def input_passphrase(self):
		response = {}

		if self.identity:
			response["Identity"] = self.identity
		if self.passphrase:
			response["Passphrase"] = self.passphrase
		if self.wpspin:
			response["WPS"] = self.wpspin

		return response

	def input_username(self):
		response = {}

		if self.username:
			response["Username"] = self.username
		if self.password:
			response["Password"] = self.password

		return response

	def input_hidden(self):
		response = {}

		if self.name:
			response["Name"] = self.name
		if self.ssid:
			response["SSID"] = self.ssid

		logging.debug('Connman: Input hidden = {}.'.format(response))
		return response

	@dbus.service.method("net.connman.Agent",
		in_signature='oa{sv}', out_signature='a{sv}')
	def RequestInput(self, path, fields):
		response = {}

		if fields.has_key("Name"):
			response.update(self.input_hidden())
		if fields.has_key("Passphrase"):
			response.update(self.input_passphrase())
		if fields.has_key("Username"):
			response.update(self.input_username())

		logging.debug('Connman: Request input = {}.'.format(response))
		return response

	@dbus.service.method("net.connman.Agent",
			in_signature='', out_signature='')
	def Cancel(self):
		pass

def property_changed(name, value):
	"""
	Signal handler for property chaned
	"""
	if name == "State":
		val = str(value)
		if val in ('ready', 'online'):
			loop.quit()
			logging.debug("Connman: Autoconnect callback.")

class ConnmanClient:
	''' @brief  : Connman client class.'''

	def __init__(self, autoconnect_timeout):

		# Setting up bus
		dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

		self.bus = dbus.SystemBus()
		self.manager = dbus.Interface(self.bus.get_object("net.connman", "/"),
			"net.connman.Manager")
		self.technology = dbus.Interface(self.bus.get_object("net.connman",
			"/net/connman/technology/wifi"), "net.connman.Technology")

		self.agent = None

		# Variables
		self.connected = False
		self.autoconnect_timeout = autoconnect_timeout
		self.error = None

	def handle_connect_error(self, error):
		loop.quit()
		self.error = error
		self.connected = False
		logging.debug("Connman: Connect error = {}.".format(error))

	def handle_connect_reply(self):
		loop.quit()
		self.error = None
		self.connected = True
		logging.debug("Connman: Connected.")

	def autoconnect_timeout_handler(self):
		loop.quit()
		self.connected = False
		logging.debug("Connman: Autoconnect timeout.")

	def scan(self):
		scanList = []
		try:
			self.technology.Scan()

			services = self.manager.GetServices()
			logging.debug('Connman: Get wifi networks.')

			for service in services:
				(path, params) = service                        
				if params['Type'] == 'wifi':                
					if 'Name' in params:
						d = {}
						logging.debug('     Found {}'.format(params['Name']))
						d['State'] = params['State']
						d['Name'] = params['Name']
						if 'psk' in params['Security']: 
						    d['Security'] = 'WPA-PSK'
						else:
						    d['Security'] = 'Open'

						d['Strength'] = '{:d}'.format(params['Strength'])
						d['Ipv4'] = params['IPv4']
						d['Ipv6'] = params['IPv6'] 
						logging.debug('         Security {}'.format(d['Security']))
						logging.debug('         RSSI     {}'.format(d['Strength']))
						scanList.append(d)

		except Exception as e:
			logging.error('Connman: Scan error = {}.'.format(e))

		return scanList

	def connect(self, ServiceId, **credentials):
		path = "/net/connman/service/" + ServiceId

		service = dbus.Interface(self.bus.get_object("net.connman", path),"net.connman.Service")

		agentpath = "/test/agent"
		# try unregister first
		try:						
			self.manager.UnregisterAgent(agentpath)
		except Exception as e:
			logging.warning("Agent: Registering exception e={}".format(e))

		# ensure agent not already registered
		self.agent = Agent(self.bus, agentpath)
		try:			
			self.manager.RegisterAgent(agentpath)
		except Exception as e:
			logging.warning("Agent: Unregister agent exception e={}".format(e))		

		if credentials.has_key("name"):
			self.agent.name = credentials["name"]
			logging.debug('Agent: Name given = {}.'.format(credentials["name"]))
		if credentials.has_key("passphrase"):
			#logging.debug('Agent: Passphrase given = {}.'.format(credentials["passphrase"]))
			self.agent.passphrase = credentials["passphrase"]
		if credentials.has_key("identity"):
			logging.debug('Agent: Identity given = {}.'.format(credentials["identity"]))
			self.agent.identity = credentials["identity"]

		service.Connect(timeout=60000,
			reply_handler=self.handle_connect_reply,
			error_handler=self.handle_connect_error)

		global loop
		loop = gobject.MainLoop()
		loop.run()

	def autoconnect(self):
		timeout = gobject.timeout_add(1000*self.autoconnect_timeout, self.autoconnect_timeout_handler)

		signal = self.bus.add_signal_receiver(property_changed,
			bus_name="net.connman",
			dbus_interface="net.connman.Service",
			signal_name="PropertyChanged")

		global loop
		loop = gobject.MainLoop()
		loop.run()

		gobject.source_remove(timeout)
		signal.remove()

	def disconnect(self, ServiceId):

		path = "/net/connman/service/" + ServiceId

		service = dbus.Interface(self.bus.get_object("net.connman", path),
			"net.connman.Service")

		try:
			service.Disconnect(timeout=60000)
		except Exception as e:
			logging.error("Connman: Remove service error = {}.".format(e))

	def remove(self, ServiceId):

		path = "/net/connman/service/" + ServiceId

		service = dbus.Interface(self.bus.get_object("net.connman", path),
			"net.connman.Service")

		try:
			service.Remove()
		except Exception as e:
			logging.error("Connman: Remove service error = {}.".format(e))

	def getState(self, ServiceId):
		for path,properties in self.manager.GetServices():
			if path == "/net/connman/service/" + ServiceId:
				return properties["State"]

	#def getServiceId(self, name, technology, security, mac_address):
	def getServiceId(self, name):
		'''for path,properties in self.manager.GetServices():
			if properties.get("Name") == name and properties.get("Type") == technology and security in properties.get("Security") and properties.get("Ethernet").get('Address') == mac_address:
				serviceId = path[path.rfind("/") + 1:]
				return serviceId
		logging.error('Connman: Get service id, service not found.')'''
		try:
			servicePath = ''
			services = self.manager.GetServices()

			for service in services:
				(path,params) = service
				if 'Name' in params:
					if params['Name'] == name:
						servicePath = path

			if servicePath == '':
				logging.warning('Connman: Get service id for {} not found.'.format(name))
				return ''

			tokens = servicePath.split('/')

			if len(tokens) != 5 or 'wifi' not in tokens[4]:
				logging.warning('Connman: Get service id for {} is not wifi service.'.format(name))
				return ''

			logging.info('Connman: Get service id for {} = {}.'.format(name, tokens[4]))
			return tokens[4]

		except Exception as e:
			logging.warning('Connman: Get service id for {} error = {}.'.format(name, e))
			return '' 


	def setConfig(self, **param):
		config = ConfigParser.RawConfigParser()
		config.optionxform = str
		config.read(CONF_FILE)

		section = "service_"+param['Name']
		config.remove_section(section)
		config.add_section(section)
		config.set(section, "Type", "wifi")
		for item in param:
			if param.has_key(item):
				config.set(section, item, param[item])

		with open(CONF_FILE, 'w') as configfile:
			config.write(configfile)

	def clearConfig(self, name):
		config = ConfigParser.RawConfigParser()
		config.read(CONF_FILE)

		section = "service_"+name
		config.remove_section(section)

		with open(CONF_FILE, 'w') as configfile:
			config.write(configfile)


def getIpAddress(iface):
	''' @fn getIpAddress
	    @brief : Get active interface IP address
	'''

	ipaddr = ''
	for i in range(3):
		
		ifname = "{}{}".format(iface,i)
		try:
			osout = os.popen('ifconfig {} | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1'.format(ifname))
			#logging.debug(osout)
			ipaddr = osout.read()
			ip = ''

			if ipaddr[:8] == '192.168.':
				ip = ipaddr.strip()

			elif ipaddr[:3] == '10.':
				ip = ipaddr.strip()
				
			elif ipaddr[:4] == '172.':
				ip = ipaddr.strip()

			logging.debug('Network: IP address for {} = {}.'.format(iface, ip))
			return ip
		except Exception as e:
			logging.warning('Network: Get ip addr error {}.'.format(e))
			return ''

def getDefaultRoute():
	''' @brief : Try determine default route.'''
	try:
		objout = os.popen('route -n | grep UG | tr -s [:blank:] | cut -d" " -f8')
		out = objout.read()
		return out.strip()
	except:
		logging.debug('Network: Unable to determine default route.')
		return ''

def routeToInterface(route):
	''' @brief : Change gw route to interface. '''

	if route == 'wlan0':
		return INTERFACE_WIFI
	elif route == 'eth0':
		return INTERFACE_ETHERNET
	elif route == 'ppp0':
		return INTERFACE_MOBILE
	else:
		return INTERFACE_UNKNOWN


def httpReply(httpObj=None, responseData={}):
	''' @brief : Return http response '''
	httpObj.setHeader('Content-Type', 'application/json')
	httpObj.setResponseCode(200, 'OK')
	httpObj.write(json.dumps(responseData).encode('UTF-8'))
	httpObj.finish()







class Network(Model):

	''' @class : Network class
		@brief : Report network interface, ip address and tx/rx stats.
	'''
	def __init__(self, eventMgr=None):
		''' @fn : __init__
			@brief : Class initialisation.
		'''
		super(Network, self).__init__('Network')
		self.eventMgr = eventMgr
		# can report metrics at different configurable frequency
		self.currentInterface = INTERFACE_UNKNOWN
		self.ipAddress = ''

		self.cc = ConnmanClient(90)

		logging.info('Network: Initialised.')

	def getLiveState(self):
		''' @brief : Get live state. '''

		status = '{0},{1}'.format(hrInterface[self.currentInterface], self.ipAddress)
		return status

	def wifiPskConnect(self, content):
		''' @brief 		: Connect to protected wifi network. Called by http shell.
			@return 	: Dictionary {"result": -1, "repr":""} '''

		details = json.loads(content)
		result = {'Result':-1, 'Repr':"Success", "Data":""}

		try:
			if 'Ssid' not in details:
				rspStr = "No SSID in details"
				result['Result'] = 1
				result['Repr'] = rspStr
				logging.warning('Network: Wifi connect, {}'.format(rspStr))
				return result

			serviceId = self.cc.getServiceId(details['Ssid'])
			if serviceId == '':
				rspStr = "No service Id"
				result['Result'] = 1
				result['Repr'] = rspStr
				logging.warning('Network: Wifi connect, {}'.format(rspStr))
				return result

			psk = ''
			if 'Psk' in details:
				psk = details['Psk']		
				self.cc.connect(serviceId, identity=serviceId, passphrase=psk)

			else:
				self.cc.connect(serviceId, identity=serviceId)
					

			if self.cc.connected:
				ipaddr = getIpAddress('wlan')
				if ipaddr != '':
					result['Result'] = 0
					result['Data'] = ipaddr
					logging.warning('Network: Wifi connect, success Ip address = {}'.format(ipaddr))
					return result
				else:
					# no ip addr ?
					rspStr = "No IP address"
					result['Result'] = 1
					result['Repr'] = rspStr
					logging.warning('Network: Wifi connect, {}'.format(rspStr))
					return result
			else:
				rspStr = "Failed to connect"
				result['Result'] = 1
				result['Repr'] = rspStr
				logging.warning('Network: Wifi connect, {}'.format(rspStr))
				return result


		except Exception as e:
			rspStr = "Exception ({})".format(e)
			result['Result'] = 1
			result['Repr'] = rspStr
			logging.warning('Network: Wifi connect, {}'.format(rspStr))
			return result
			

	def wifiScan(self):
		''' @brief 	: Http request from shell to scan wifi and return list. '''
		
		result = {'Result':0, "Repr": "Success", "Data":{}}

		wifiNets = self.cc.scan()
		logging.debug('Network: Wifinets = {}'.format(wifiNets))
		result["Data"]["Networks"] = wifiNets
		result["Data"]["Total"] = len(wifiNets)
		return result


	def testWifiConnect(self, name, psk):
		serviceId = self.cc.getServiceId(name)
		self.cc.connect(serviceId, identity=serviceId, passphrase=psk)
		if self.cc.connected:
			ipaddr = getIpAddress('wlan')
		else:
			logging.debug('Network: Test wifi connect failed.')

	def run(self):
		''' @brief : Executes in the thread. Checks network metrics. '''

		lasttime = 0
		while True:

			time.sleep(1)

			if self.stopThread:
				break

			if time.time() - lasttime < 30:
				continue

			gwRoute = getDefaultRoute()
			logging.debug('Network: Default GW route = {}.'.format(gwRoute))
			if gwRoute != '':
				newInterface = routeToInterface(gwRoute)
				if newInterface != self.currentInterface:
					self.eventMgr.raiseEvent(tgEvent.EVCLASS_NETWORK, 
											tgEvent.EVTOPIC_NET_IFACECHANGE, 
											'{},{}'.format(hrInterface[ self.currentInterface ], hrInterface[ newInterface ]), 
											tgEvent.EVENT_PRIORITY_HIGH, 
											[tgEvent.EVACTION_STREAM, tgEvent.EVACTION_DATABASE, tgEvent.EVACTION_NOTIFICATIONS])
					
				self.currentInterface = newInterface	

				# cut the number off
				self.ipAddress = getIpAddress(gwRoute[:-1])

			else:
				# from some interface to no interface
				if self.currentInterface != INTERFACE_UNKNOWN:
					self.eventMgr.raiseEvent(tgEvent.EVCLASS_NETWORK, 
											tgEvent.EVTOPIC_NET_IFACECHANGE, 
											'{},{}'.format(hrInterface[ self.currentInterface ], hrInterface[ INTERFACE_UNKNOWN ]), 
											tgEvent.EVENT_PRIORITY_HIGH, 
											[tgEvent.EVACTION_STREAM, tgEvent.EVACTION_DATABASE, tgEvent.EVACTION_NOTIFICATIONS])
				self.currentInterface = INTERFACE_UNKNOWN
				self.ipAddress = ''


			lasttime = time.time()






if __name__ == '__main__':
	
	argc = len(sys.argv)

	# root logger
	logger = logging.getLogger('')
	logger.setLevel(logging.DEBUG)
	
	# format for logging
	format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

	# output to stdout
	stdouth = logging.StreamHandler(sys.stdout)
	stdouth.setFormatter(format)
	logger.addHandler(stdouth)

	if argc >= 3 and sys.argv[1] == '--wifi':
		if argc == 5 and sys.argv[2] == 'connect':
			net = Network()
			net.testWifiConnect(sys.argv[3], sys.argv[4])
			#cc = ConnmanClient(90)
			#cc.scan()
			#serviceId = cc.getServiceId(sys.argv[3])
			#cc.connect(serviceId, identity=serviceId, passphrase=sys.argv[4])
			#if cc.connected:
			#	print 'Connected to ' + sys.argv[3]

		elif sys.argv[2] == 'list':
			cc = ConnmanClient(90)
			cc.scan()


	elif sys.argv[1] == '--ipaddr' and argc == 3:
		getIpAddress(sys.argv[2])



	

	
			