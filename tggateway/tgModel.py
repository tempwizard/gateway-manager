#!/usr/bin/env python3

''' @file : tgModel.py
	@brief : Model.
'''

import threading
import logging

class Model(object):
	''' @class : Model
		@brief : Model for common classes that use a thread.
	'''
	def __init__(self, name):
		''' @fn : __init__
			@brief : Class initialisation.
		'''
		# thread related
		self.name = name
		self.stopThread = False
		self.thread = None 

	def start(self):
		''' @fn : start
			@brief : Start thread.
		'''	
		self.stopThread = False
		logging.debug('{0}: Starting new thread.'.format(self.name))
		self.thread = threading.Thread(target=self.run, args=(), name=self.name)
		self.thread.daemon = True
		self.thread.start()
			
	def stop(self):
		''' @fn : stop
			@brief : Stop thread.
		'''	
		self.stopThread = True
		logging.debug('{0}: Stopping thread.'.format(self.name))
		if self.thread is not None:
			if self.thread.is_alive():
				self.thread.join()