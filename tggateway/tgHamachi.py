#!/usr/bin/env python3

''' @file : tgHamachi.py
	@brief : Hamachi client.
'''
import sys
import os
import logging
import logging.handlers
import subprocess
import tgEvent
import json
import copy
import time
from tggateway.tgModel import Model

HAMACHID_OFF		= 0
HAMACHID_RUNNING	= 1

HAMACHI_ERR         = 0
HAMACHI_NOT_RUNNING = 0
HAMACHI_OFFLINE     = 0
HAMACHI_LOGGING_IN  = 0
HAMACHI_LOGGED_IN   = 1

HAMACHI_CONFIG_FILE = '/home/tgard/config/hamachi.json'


class Hamachi(Model):
	def __init__(self, eventMgr):

		super(Hamachi, self).__init__('Hamachi')

		self.eventMgr = eventMgr
		self.daemon = HAMACHID_OFF
		self.state = HAMACHI_OFFLINE
		self.config = {}
		self.configured = False

		logging.info('Hamachi: Initialised.')

	def daemonState(self):
		try:
			p = subprocess.Popen(['ps', 'axf'], stdout=subprocess.PIPE)
			o, e = p.communicate()

			if '/opt/logmein-hamachi/bin/hamachid'.encode('utf-8') in o:
				logging.debug('Hamachi: Daemon running.')
				self.daemon = HAMACHID_RUNNING
				return True
			else:
				logging.debug('Hamachi: Daemon not running.')
				self.daemon = HAMACHID_OFF
				return False

		except Exception as e:
			logging.error("Hamachi: Unexpected error {0}.".format(e))
			return False 

	def daemonStart(self):
		try:
			p = subprocess.Popen(['service', 'logmein-hamachi', 'start'], stdout=subprocess.PIPE)
			out,err = p.communicate()
			logging.debug('Hamachi: Service start.')
		except Exception as e:
			logging.error('Hamachi: Service start error {0}.'.format(e))
			return False

	def daemonStop(self):
		try:
			p = subprocess.Popen(['service', 'logmein-hamachi', 'stop'], stdout=subprocess.PIPE)
			out,err = p.communicate()
			logging.debug('Hamachi: Service stop.')
		except Exception as e:
			logging.error('Hamachi: Service stop error {0}.'.format(e))


	def login(self):
		try:		
			p = subprocess.Popen(['hamachi', 'login'], stdout=subprocess.PIPE)
			out,err = p.communicate()
			logging.info('Hamachi: Login.')
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))			

	def logout(self):
		try:
			p = subprocess.Popen(['hamachi', 'logout'], stdout=subprocess.PIPE)
			out,err = p.communicate()
			logging.info('Hamachi: Logout.')
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))

	def join(self, vpnId, password):
		try:
			logging.info('Hamachi: Join {0}.'.format(vpnId))
			p = subprocess.Popen(['hamachi', 'join', vpnId, password], stdout=subprocess.PIPE)
			out,err = p.communicate()
			self.eventMgr.raiseEvent(tgEvent.EVCLASS_HAMACHI, tgEvent.EVTOPIC_HAM_JOINED, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))

	def leave(self, vpnId):
		try:
			logging.info('Hamachi: Leave {0}.'.format(vpnId))
			p = subprocess.Popen(['hamachi', 'leave', vpnId], stdout=subprocess.PIPE)
			out,err = p.communicate()
			self.eventMgr.raiseEvent(tgEvent.EVCLASS_HAMACHI, tgEvent.EVTOPIC_HAM_LEAVE, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))

	def setNickname(self, name):
		try:
			logging.info('Hamachi: Set-nick: {0}.'.format(name))
			p = subprocess.Popen(['hamachi', 'set-nick', name], stdout=subprocess.PIPE)
			out,err = p.communicate()
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))

	def factoryReset(self):
		try:
			logging.info('Hamachi: Factory reset.')
			self.logout()
			self.daemonStop()
			# remove logmein-hamachi config files
			p = subprocess.Popen(['rm', '/usr/lib/logmein-hamachi/*'], stdout=subprocess.PIPE)
			p.communicate()
			# remove config file
			p = subprocess.Popen(['rm', HAMACHI_CONFIG_FILE], stdout=subprocess.PIPE)
			p.communicate()

			return True
		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))
			return False

	def loadConfig(self):
		'''
			@brief : Load config from file.
		'''
		try:
			with open(HAMACHI_CONFIG_FILE, 'r') as f:
				self.config = json.load(f)

			logging.debug('Hamachi config:')
			logging.debug('	Net ID: {0}.'.format(self.config['NetId']))
			logging.debug('	Client ID: {0}.'.format(self.config['ClientId']))
			logging.debug('	Join pass: {0}.'.format(self.config['NetPass']))
			logging.debug('	Ip address: {0}.'.format(self.config['Ipv4']))
			logging.debug('	Version: {0}.'.format(self.config['Version']))
			#logging.debug(' Report stats: {0}.'.format(self.config['Report']['Stats']))
			#logging.debug(' Report status: {0}.'.format(self.config['Report']['Status']))
			
			logging.info('Hamachi: Config loaded from file.')
			self.configured = True

		except Exception as e:
			logging.error("Hamachi: Load config error {0}.".format(e))
			self.eventMgr.raiseEvent(tgEvent.EVCLASS_HAMACHI, tgEvent.EVTOPIC_HAM_NO_CONFIG, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])


	def dumpConfig(self):
		'''
			@brief : Dump config to file.
		'''
		try:
			with open(HAMACHI_CONFIG_FILE, 'w') as f:
				json.dump(device, f)
		
			logging.info('Hamachi: Config dumped to file.')
		
		except Exception as e:
			logging.error("Hamachi: Dump config error {0}.".format(e))

	def setConfig(self, config):
		''' 
			@brief : Set config.
		'''
		backup = copy.deepcopy(self.config)
		self.config.clear()
		try:	
			self.config	= config
			
			# validate 

			self.dumpConfig()

			logging.debug('Hamachi: Config set.')

			self.configured = True
			return True
		except Exception as e:
			logging.error('Hamachi: Config set error {0}.'.format(e))
			# restore previous
			self.config = copy.deepcopy(backup)
			return False

	def getConfig(self):
		''' 
			@brief : Get config.
		'''
		return copy.deepcopy(self.config)

	def getLiveState(self):
		''' 
			@brief : Get config.
		'''
		return copy.copy(self.state)

	def report(self):
		''' @brief : Report hamachi status. '''
		d = {}
		try:
			# the extra cut in the address command is for ipv4 and ipv6 address separated by space
			ob = os.popen('hamachi | grep address | tr -s [:blank:] | cut -d":" -f2 | cut -d" " -f2')
			d['Address'] = ob.read() 

			ob = os.popen('hamachi | grep version | tr -s [:blank:] | cut -d":" -f2')
			d['Version'] = ob.read() 
			
			ob = os.popen('hamachi | grep client | tr -s [:blank:] | cut -d":" -f2')
			d['ClientId'] = ob.read()
			
			ob = os.popen('hamachi | grep status | tr -s [:blank:] | cut -d":" -f2')
			d['Status'] = ob.read() 

			return d
		except Exception as e:
			logging.debug('Hamachi: Unable to parse output error = {}.'.format(e))
			return {}

	def status(self):

		try:

			if not self.daemonState():
				self.daemonStart()

			p = subprocess.Popen(["hamachi"], stdout=subprocess.PIPE)
			out, err = p.communicate()
		
			#logging.debug('Hamachi: Status: {0}'.format(out))

			if 'logged in' in out:
				if self.state == HAMACHI_OFFLINE:
					self.eventMgr.raiseEvent(tgEvent.EVCLASS_HAMACHI, tgEvent.EVTOPIC_HAM_ONLINE, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])
				self.state = HAMACHI_LOGGED_IN
			elif 'logging in' in out:
				self.state = HAMACHI_LOGGING_IN
			else:
				if self.state != HAMACHI_OFFLINE:
					self.eventMgr.raiseEvent(tgEvent.EVCLASS_HAMACHI, tgEvent.EVTOPIC_HAM_OFFLINE, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])
				self.state = HAMACHI_OFFLINE

		except Exception as e:
			logging.error('Hamachi: Unexpected error {0}.'.format(e))

	def run(self):
		
		self.status()
		lasttime = 0
		stillLoggingIn = 0

		while True:

			time.sleep(1)

			if self.stopThread:
				break
			
			if time.time() - lasttime < 30:
				continue

			self.status()

			if self.state == HAMACHI_OFFLINE:
				self.login()
				stillLoggingIn = 0

			elif self.state == HAMACHI_LOGGING_IN:
				# if hamachi saying its still logging in after 2 minutes, try logout and then login again.
				stillLoggingIn += 1
				if stillLoggingIn == 3:
					stillLoggingIn = 0
					self.logout()
					self.login()
			else:
				stillLoggingIn = 0

			lasttime = time.time()

			



def cleanExit():
	''' @fn cleanExit
		@brief : Clean exit handler when signal terminates program.
	'''	
	logging.info('Temperature: Exiting application.')
	sys.exit()

def fSignalHandler(signal, frame):		
	''' @fn fSignalHandler
		@brief : Signal handler.
	'''
	cleanExit()

if __name__ == '__main__':
	
	argc = len(sys.argv)	

	# root logger
	logger = logging.getLogger('')
	logger.setLevel(logging.DEBUG)
	
	# format for logging
	format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

	# add stdout stream handler
	stdouth = logging.StreamHandler(sys.stdout)
	stdouth.setFormatter(format)
	logger.addHandler(stdouth)

	tgEvt = tgEvent.Event()
	tgHam = Hamachi(tgEvt)
	tgHam.loadConfig()

	if sys.argv[1] == '--daemon-state':
		tgHam.daemonState()

	elif sys.argv[1] == '--daemon-start':
		tgHam.daemonStart()

	elif sys.argv[1] == '--daemon-stop':
		tgHam.daemonStop()

	elif sys.argv[1] == '--login':
		tgHam.login()

	elif sys.argv[1] == '--logout':
		tgHam.logout()

	elif sys.argv[1] == '--join':
		if argc == 4:
			tgHam.join(sys.argv[2], sys.argv[3])

	elif sys.argv[1] == '--leave':
		if argc == 3:
			tgHam.leave(sys.argv[2])

	elif sys.argv[1] == '--set-nick':
		if argc == 3:
			tgHam.setNickname(sys.argv[2])

	elif sys.argv[1] == '--reset':
		tgHam.factoryReset()

	elif sys.argv[1] == '--status':
		tgHam.report()
		if tgHam.state == HAMACHI_LOGGED_IN:
			logging.debug('Hamachi: logged in.')		
		elif tgHam.state == HAMACHI_OFFLINE:
			logging.debug('Hamachi: logged out.')
		elif tgHam.state == HAMACHI_LOGGING_IN:
			logging.debug('Hamachi: logging in.')
		else:
			logging.debug('Hamachi: Unknown state.')
