#!/usr/bin/env python2

''' @file : tgShell.py
	@brief : Shell.
'''

import os
import sys
import logging
import logging.handlers
import signal
import threading
import time
import copy
import json
import subprocess

from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor, task

import tggateway.tgEvent as tgEvent
from tggateway.tgHamachi import Hamachi
from tggateway.tgTemperature import Temperature
from tggateway.tgNotification import Notification
from tggateway.tgAudioVisual import AudioVisual
from tggateway.tgGateway import Gateway
from tggateway.tgSystem import System
from tggateway.tgEmail import Email

#from tggateway.tgSms import Sms
from tgNetwork import Network


INFO_ALL = -1
INFO_STATE = 0
INFO_TIMEZONE = 1
INFO_STORAGE = 2
INFO_CONN_INTERFACE = 3
INFO_CONN_IP = 4
INFO_POWER_STATE = 5
INFO_SERVER_VERSION = 6
INFO_LCD_VERSION = 7
INFO_FIRST = INFO_STATE
INFO_LAST = INFO_LCD_VERSION
INFO_TOTAL = INFO_LAST + 1

IDENTITY_ALL = -1
IDENTITY_SERIAL = 0
IDENTITY_NAME = 1
IDENTITY_D1 = 2
IDENTITY_D2 = 3
IDENTITY_D3 = 4
IDENTITY_D4 = 5
IDENTITY_D5 = 6
IDENTITY_TOTAL = 7

SESSION_ALL = -1
SESSION_NUMBER = 0
SESSION_ALIAS = 1
SESSION_SIZE = 2
SESSION_LOGRATE = 3
SESSION_ALARM_TYPE = 4
SESSION_TRIGGER_RATE = 5
SESSION_TOTAL = 6

SENSOR_ALL = -1
SENSOR_POS = 0
SENSOR_SERIAL = 1
SENSOR_ALIAS = 2
SENSOR_ALARM_1 = 3
SENSOR_ALARM_2 = 4
SENSOR_DIFF_MODE = 5
SENSOR_TOTAL = 6

RESPONSE_INVALID_PACKET = 666
RESPONSE_RESULT_SUCCESS = 0
RESPONSE_RESULT_FAIL = 1
RESPONSE_INTERNAL_ERROR = 2

# Publish events to LCD
PUB_CONNECT = 0
PUB_DISCONNECT = 1
PUB_DATA = 3

SIMPLE_SHELL_PORT = 54213
SHELL_PORT = 54313
STREAM_PORT = 54113

DIR_SESSION_ARCHIVE = '/home/tgard/session/{}'
FILE_CONFIG_TEMPERATURE = '/home/tgard/config/temperature.json'
FILE_ARCHIVE_TEMPERATURE = '/home/tgard/session/{}/temperature.json'
FILE_KEY = "/home/tgard/config/.key"

SHUTDOWN_REASON_LOW_BATTERY = 'Low battery'
SHUTDOWN_REASON_USER = 'User'


HTTP_REQUEST_ALL_CONFIG                = "/configAll"
HTTP_REQUEST_WIFI_SCAN                 = "/wifiScan"
HTTP_REQUEST_WIFI_CONNECT              = "/wifiConnect"
HTTP_REQUEST_SESSION_RESUME            = "/sessionResume"
HTTP_REQUEST_SESSION_RESTART           = "/sessionRestart"
HTTP_REQUEST_SESSION_NEW               = "/sessionNew"
HTTP_REQUEST_TEMP_RECORDS              = "/dataRecords"
HTTP_REQUEST_USERS                     = "/users"
HTTP_REQUEST_ID                        = "/identity"
HTTP_REQUEST_POWER_OFF		           = "/powerOff"
HTTP_REQUEST_MUTE_ALARM                = "/audioMute"
HTTP_REQUEST_EMAIL_IMAGES              = "/emailImages"
HTTP_REQUEST_AV_CONFIG				   = "/audioVisual"
HTTP_REQUEST_CREDENTIALS			   = "/credentials" 
HTTP_REQUEST_KEY					   = "/key"


system = None
event = None
temp = None 
gateway = None
hamachi = None
notification = None
email = None
audiovisual = None
simple = None
shell = None
network = None

def uFileExists(file):
	''' @fn : uFileExists
		@brief : Check if file exists at given absolute path.
		@param filepath : Full path of file.
	'''

	if os.path.isfile(file) and os.access(file, os.R_OK):
		logging.debug('FileExists: {0} ? = yes.'.format(file))
		return True
	else:
		logging.debug('FileExists: {0} ? = no.'.format(file))
		return False

def uFileRemove(file):
	''' @fn : uFileRemove
		@brief : Remove file if it exists.
		@param : Full path to file.
	'''
	if uFileExists(file):
		p = subprocess.Popen(['rm', file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		o,e = p.communicate()
		if not uFileExists(file):
			logging.debug('FileRemove: {0} ? = yes.'.format(file))
			return True
		else:
			logging.warning('FileRemove: {0} ? = no.'.format(file))
			return False
	else:
		logging.warning('FileRemove: {0} does not exist.'.format(file))
		return False

def uFileMove(src, dst):
	''' @brief : Move file from src to dst'''
	
	p = subprocess.Popen(['mv', src, dst], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	o,e = p.communicate()
	if uFileExists(dst):
		logging.debug('FileMove: From {0} to {1} ? = yes.'.format(src, dst))
		return True
	else:
		logging.warning('FileMove: From {0} to {1} ? = no.'.format(src, dst))
		return False

def uFileCopy(src, dst):
	''' @brief : Copy file from src to dst'''
	if uFileExists(src):
		p = subprocess.Popen(['cp', src, dst], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		o,e = p.communicate()
		if uFileExists(dst):
			logging.debug('FileCopy: From {0} to {1} ? = yes.'.format(src, dst))
			return True
		else:
			logging.warning('FileCopy: From {0} to {1} ? = no.'.format(src, dst))
			return False
	else:
		logging.debug('FileCopy: Src {} does not exist.'.format(src))
		return False 

def uMkDir(dir):
	''' @brief : Make new directory '''
	if os.path.isdir(dir):
		return True

	p = subprocess.Popen(['mkdir', dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	o,e = p.communicate()
	if os.path.isdir(dir):
		logging.debug('MkDir: {} ? = yes.'.format(dir))
		return True
	else:
		logging.warning('MkDir: {} ? = no.'.format(dir))
		return False

def uBzip(src,dst):
	''' @brief : Compress src and put at dst with bzip2 (and keep the src)'''
	result = os.system('bzip2 < {} > {}'.format(src,dst))
	if result == 0:
		return True
	else:
		return False

def httpResponse(result, httpRequest):
	''' @brief : Generic success response for any GET/POST method. '''
	httpRequest.setHeader('Content-Type', 'application/json')
	httpRequest.setResponseCode(200, 'OK')
	httpRequest.write(json.dumps(result).encode('UTF-8'))
	httpRequest.finish()

# GET methods

def getAllCfg(httpRequest):
	''' @fn : getAllCfg
		@brief : Get all configuration.
	'''

	response = {"Result":0, "Request": HTTP_REQUEST_ALL_CONFIG, "Repr":"Success", "Data":{}}
	logging.debug('GET: All config.')
	
	try:				
		response['Data']['Temp'] = temp.getConfig()
		response['Data']['Creds'] = gateway.getConfig()
		response['Data']['Ham'] = hamachi.getConfig()
		response['Data']['Users'] = notification.getConfig()
		response['Data']['Email'] = email.getConfig()
		response['Data']['Av'] = audiovisual.getConfig()

		key = ""
		if uFileExists(FILE_KEY):
			with open(FILE_KEY) as f:
				key = f.read()

		response['Data']['Key'] = key

	except Exception as e:
		logging.error('GET: All config error {0}.'.format(e))
		response['Result'] = 1
		response["Repr"] = e

	httpResponse(response, httpRequest)
		
def getGwyId(httpRequest):
	''' @fn : getGwyId
		@brief : Get gateway id.
	'''
	response = {"Result":0, "Request": HTTP_REQUEST_ID, "Repr":"Success", "Data":{}}
	logging.debug('GET: Gateway id.')

	try:		
		response["Data"] = gateway.getConfig()['Id']
	except Exception as e:
		logging.error('GET: Gateway id error {}.'.format(e))
		response['Result'] = 1
		response["Repr"] = e
		
	httpResponse(response, httpRequest)

def getNetWifiScan(httpRequest):
	''' @brief 	: Wifi net scan. '''

	response = {"Result":0, "Request": HTTP_REQUEST_WIFI_SCAN, "Repr":"Success", "Data":{}}
	logging.debug('GET: Network wifi scan.')
	result = network.wifiScan()
	response["Result"] = result["Result"]
	response["Repr"] = result["Repr"]
	response["Data"] = result["Data"]
	httpResponse(response, httpRequest)


def getDataRecords(httpRequest):
	''' @brief : Get data records '''

	# request.args => {'current':""}

	try:

		session = httpRequest.args['session'][0]
		filename = httpRequest.args['file'][0]

		logging.debug('GET: Get records session={} file={}.'.format(session, filename))
		index = 0

		sessionPath = "/home/tgard/session/{}".format(session)
		if not os.path.exists(sessionPath):
			response = {"Result":1, "Request": HTTP_REQUEST_TEMP_RECORDS, "Repr":"Session does not exist"}
			httpResponse(response, httpRequest)
			logging.warning("Session does not exist")
			return

		# get sorted directory list
		dirList = os.listdir(sessionPath)
		os.chdir(sessionPath)
		sortedDirList = sorted(dirList, key=os.path.getmtime)
		# sort puts in oldest data file to latest data file

		totalFiles = len(sortedDirList)

		if totalFiles == 0:
			logging.warning("GET data: No data yet")
			response = {"Result":2, "Request": HTTP_REQUEST_TEMP_RECORDS, "Repr":"No data yet"}
			httpResponse(response, httpRequest)
			return


		# in case client has no files
		if filename == "none":
			index = 0

		else:
			# find index of current file in sorted dir list
			for f in sortedDirList:
				if f == filename:
					break

				index += 1

		ts = int(time.time() * 1000000)
		archiveFile = "tdata-{}.tar.gz".format(ts)

		# tar cmd to run in subprocess
		cmdToRun = ['tar', '-C', sessionPath, '-czf', '/home/tgard/requests/{}'.format(archiveFile)]
		
		# data files to put in archive
		for i in range(index, totalFiles):
			cmdToRun.append(sortedDirList[i])

		logging.debug("Tar process cmd to run = {}".format(cmdToRun))

		p = subprocess.Popen(cmdToRun,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		o,e = p.communicate()

		archiveFileName = '/home/tgard/requests/{}'.format(archiveFile)
		filesize = os.stat(archiveFileName).st_size
		logging.debug('Archive file size={}.'.format(filesize))
		httpRequest.setHeader('Content-Type', 'application/octet-stream')
		httpRequest.setHeader('Content-Length', filesize)
		httpRequest.setResponseCode(200, 'OK')
		with open(archiveFileName, 'rb') as fd:
			httpRequest.write(fd.read())

		httpRequest.finish()

		# remove the file
		uFileRemove(archiveFileName)

	except Exception as e:
		logging.error('GET: Database query error {}.'.format(e))
		# failure

		response = {"Result":1, "Request": HTTP_REQUEST_TEMP_RECORDS, "Repr":e}
		httpResponse(response, httpRequest)

# POST methods

def postGwyId(httpRequest):
	''' @fn : postGwyId
		@brief :
	'''
	response = {"Result":0, "Request": HTTP_REQUEST_ID, "Repr":"Success"}
	
	try:
		d = json.loads(httpRequest.content.getvalue())
		logging.debug('POST: Gateway id {}.'.format(d['Id']))
		config = gateway.getConfig()
		config['Id'] = d['Id']
		gateway.setConfig(config)

	except Exception as e:
		logging.error('POST: Gateway id error {}.'.format(e))
		# failure
		response['Result'] = 1
		response["Repr"] = e

	httpResponse(response, httpRequest)

def postTmpSeshNew(httpRequest):
	''' @fn : postTmpSeshNew
		@brief :
	'''
	
	# stop session called before this ?
	# start session called after this  ?
	response = {"Result":0, "Request": HTTP_REQUEST_SESSION_NEW, "Repr":"Success", "Data":""}
	logging.debug('POST: Temperature session new.')
	try:

		# dump last 
		temp.halt()

		# data in content
		d = json.loads(httpRequest.content.getvalue())

		# get current config
		config = temp.getConfig()
		oldSessionNo = config['Session']['Number']

		# copy temperature.json to old session
		uFileCopy(FILE_CONFIG_TEMPERATURE, FILE_ARCHIVE_TEMPERATURE.format(oldSessionNo))

		# new
		sessionNumber = int(time.time())
		logging.debug('POST: Temperature session new complete. Number = {}'.format(sessionNumber))

		d['Session']['Number'] = '{}'.format(sessionNumber)
		logging.debug('POST: Temperature session new complete config {}.'.format(d['Session']))
		logging.debug('POST: Temperature sensor new complete config {}.'.format(d['Sensors']))

		# set new config
		config['Session'] = copy.copy(d['Session'])
		config['Sensors'] = copy.deepcopy(d['Sensors'])
		if not temp.setConfig(copy.deepcopy(config)):
			response['Result'] = 1
			response["Repr"] = "Change config failed"
		else:

			# event NOTE: serial data to string for event, just need number and name
			eventData = '{},{}'.format(config['Session']['Number'], config['Session']['Alias'])
			event.raiseEvent(tgEvent.EVCLASS_TEMP, tgEvent.EVTOPIC_TEMP_NEW_SESH, eventData, tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_STREAM, tgEvent.EVACTION_NOTIFICATIONS])

			# resume
			temp.resume()

			logging.debug('POST: Temperature session new complete {}.'.format(config['Session']['Number']))

			response["Data"] = sessionNumber

	except Exception as e:
		logging.error('POST: Temperature session new error {}.'.format(e))
		# failure
		response['Result'] = 1
		response['Repr'] = e
		
	httpResponse(response, httpRequest)

def postTmpSeshRestart(httpRequest):
	''' @fn : postTmpSeshRestart
		@brief :
	'''

	logging.debug('POST: Temperature session restart.')
	response = {"Result":0, "Request": HTTP_REQUEST_SESSION_RESTART, "Repr":"Success", "Data":""}

	try:
		temp.halt()

		# get current config
		config = temp.getConfig()
		oldSessionNo = config['Session']['Number']

		# copy temperature.json to old session
		uFileCopy(FILE_CONFIG_TEMPERATURE, FILE_ARCHIVE_TEMPERATURE.format(oldSessionNo))

		# new
		sessionNumber = int(time.time())
		logging.debug('POST: Temperature restart session. Number = {}'.format(sessionNumber))

		

		config['Session']['Number'] = '{}'.format(sessionNumber)

		# set new config
		if not temp.setConfig(copy.deepcopy(config)):
			# need to rollback here if can't set new config
			response['Result'] = 1
			response["Repr"] = "Change config failed"
		else:

			# event NOTE: serial data to string for event, just need number and name
			eventData = '{},{}'.format(config['Session']['Number'], config['Session']['Alias'])
			event.raiseEvent(tgEvent.EVCLASS_TEMP, tgEvent.EVTOPIC_TEMP_RESTART_SESH, eventData, tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_STREAM, tgEvent.EVACTION_NOTIFICATIONS])

			# start
			temp.resume()

			logging.debug('POST: Temperature session restart complete {}.'.format(config['Session']['Number']))

			response["Data"] = sessionNumber

	except Exception as e:
		logging.error('POST: Temperature session restart error {}.'.format(e))
		# failure
		response['Result'] = 1
		response["Repr"] = e
		
	httpResponse(response, httpRequest)

def postTmpSeshResume(httpRequest):
	''' @brief : Resume session after change setting'''

	response = {"Result":0, "Request": HTTP_REQUEST_SESSION_RESUME, "Repr":"Success"}
	logging.debug('POST: Temperature session resume.')
	try:
		# data in content
		d = json.loads(httpRequest.content.getvalue())

		# get current config
		config = temp.getConfig()

		logging.debug('POST: Temperature session resume config {}.'.format(d['Session']))
		logging.debug('POST: Temperature sensor resume {}.'.format(d['Sensors']))

		# serialise event data
		# check for the changes, so do before set new config
		eventData = '{},{},{},{}'.format(config['Session']['Number'], d['Session']['Alias'], config['Session']['AlarmType'], d['Session']['TriggerRate'])
		for i in range(config['Session']['TotalSensors']):
			pos = '{}'.format(i+1)
			if d['Sensors'][pos]['Alias'] != config['Sensors'][pos]['Alias'] or d['Sensors'][pos]['A1'] != config['Sensors'][pos]['A1'] or d['Sensors'][pos]['A2'] != config['Sensors'][pos]['A2']:
				config['Sensors'][pos] = copy.deepcopy(d['Sensors'][pos])
				eventData += ',{},{},{},{}'.format(pos, config['Sensors'][pos]['Alias'], config['Sensors'][pos]['A1'], config['Sensors'][pos]['A2'])

		# set new config
		if d['Session']['Alias'] != config['Session']['Alias']:
			config['Session']['Alias'] = d['Session']['Alias']	

		if d['Session']['TriggerRate'] != config['Session']['TriggerRate']:
			config['Session']['TriggerRate'] = d['Session']['TriggerRate']

		if not temp.changeConfig(config):
			response["Repr"] = "Change config failed"
			response['Result'] = 1
		else:
			event.raiseEvent(tgEvent.EVCLASS_TEMP, tgEvent.EVTOPIC_TEMP_RESUME_SESH, eventData, tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_STREAM, tgEvent.EVACTION_DATABASE, tgEvent.EVACTION_NOTIFICATIONS])

	except Exception as e:
		logging.error('POST: Temperature session resume error {}.'.format(e))
		# failure
		response["Repr"] = e
		response['Result'] = 1

	httpResponse(response, httpRequest)

def postNfyCfg(httpRequest):
	''' @fn : postNfyCfg
		@brief :
	'''

	logging.debug('POST: Users config')
	response = {"Result":0, "Request": HTTP_REQUEST_USERS, "Repr":"Success"}
	
	try:
		d = json.loads(httpRequest.content.getvalue())
		if not notification.setConfig(d):
			response['Result'] = 1
			response["Repr"] = "Change config failed"

	except Exception as e:
		logging.error('POST: Notification set config error {}.'.format(e))
		# failure
		response["Repr"] = e
		response['Result'] = 1

	httpResponse(response, httpRequest)

def postNetWifiConnect(httpRequest):
	''' @brief 	: Wifi connect '''
	logging.debug('POST: Network wifi connect')
	result = network.wifiPskConnect(httpRequest.content.getvalue())
	response = {"Result":result["Result"], 
				"Request": HTTP_REQUEST_WIFI_CONNECT, 
				"Repr": result["Repr"],
				"Data": result["Data"]}

	httpResponse(response, httpRequest)

def postEmailImages(httpRequest):
	''' @fn : postEmailImages
		@brief : Email images to recipients 
	'''
	response = {"Result":0, "Request": HTTP_REQUEST_EMAIL_IMAGES, "Repr":"Success"}
	logging.debug('POST: Email images {}.'.format(httpRequest))
	try:
		d = json.loads(httpRequest.content.getvalue())
		if "AddressList" not in d:
			result["Result"] = 1
			result["Repr"] = "No AddressList in request"

		elif len(d["AddressList"]) == 0:
			result["Result"] = 1
			result["Repr"] = "No addresses in request"

		elif "ImagePaths" not in d:
			result["Result"] = 1
			result["Repr"] = "No ImagePaths in request"

		elif len(d["ImagePaths"]) == 0:
			result["Result"] = 1
			result["Repr"] = "No images in request"

		else:
			result = email.mailImages(d["AddressList"], d["ImagePaths"])
			if not result:
				result["Result"] = 1
				result["Repr"] = "Mail images failed"
		
	except Exception as e:
		logging.error('POST: Email images error = {}.'.format(e))
		# failure
		response["Repr"] = e
		response['Result'] = 1
		
	httpResponse(response, httpRequest)

def postSysPowerOff(httpRequest):
	'''
		@brief : Power restart system.
	'''
	logging.debug('POST: System power reboot={}.'.format(httpRequest))
	response = {"Result":0, "Request": HTTP_REQUEST_POWER_OFF, "Repr":"Success"}
	reboot = httpRequest.args['reboot'][0]
	if reboot == "true":
		system.reboot()
		exitApp(SHUTDOWN_REASON_USER)
	elif reboot == "false":
		system.shutdown()
		exitApp(SHUTDOWN_REASON_USER)
	else:
		response["Repr"] = "No reboot arg"
		response["Result"] = 1

	httpResponse(result, httpRequest)

def postAvMute(httpRequest):
	''' @brief : Mute audio. '''
	logging.debug('POST: Av mute.')
	response = {"Result":0, "Request": HTTP_REQUEST_MUTE_ALARM, "Repr":"Success"}
	audiovisual.alert(tgEvent.EVTOPIC_AUV_MUTE)
	httpResponse(response, httpRequest)

def postAvConfig(httpRequest):

	logging.debug("POST: Set AV config")
	response = {"Result":0, "Request": HTTP_REQUEST_AV_CONFIG, "Repr":"Success"}
	d = json.loads(httpRequest.content.getvalue())
	result = audiovisual.setConfig(d)
	if not result:
		response["Result"] = 1
		response["Repr"] = "Config not valid"
	else:

		# Bit of a hack so audio can digest new settings
		# and activate I/O if required

		# get alarm state {0},{1},{2}
		gAlarmState = temp.getGlobalAlarmState()
		logging.debug("POST: Set AV config, alarm state = {} ".format(gAlarmState))
		if gAlarmState == 3:
			audiovisual.alert(tgEvent.EVTOPIC_AUV_STATE_A1)
		elif gAlarmState == 4:
			audiovisual.alert(tgEvent.EVTOPIC_AUV_STATE_A2)
		else:
			audiovisual.alert(tgEvent.EVTOPIC_AUV_STATE_A0)

	httpResponse(response, httpRequest)

def postCredentials(httpRequest):
	logging.debug("POST: Post credentials")
	response = {"Result":0, "Request": HTTP_REQUEST_CREDENTIALS, "Repr":"Success"}
	oldConfig = gateway.getConfig()
	d = json.loads(httpRequest.content.getvalue())
	result, repr = gateway.setConfig(d)

	if not result:
		response["Result"] = 1
		response["Repr"] = repr
	else:
		# compare name and update hamachi if changed
		newConfig = gateway.getConfig()

		if newConfig["Id"]["Alias"] != oldConfig["Id"]["Alias"]:
			logging.info("Gateway name changed from {} to {}, update Hamachi nickname too".format(oldConfig["Id"]["Alias"], newConfig["Id"]["Alias"]))
			hamachi.setNickname(newConfig["Id"]["Alias"])


	httpResponse(response, httpRequest)

def postKey(httpRequest):
	logging.debug("POST: Key")
	response = {"Result":0, "Request": HTTP_REQUEST_KEY, "Repr":"Success"}
	d = json.loads(httpRequest.content.getvalue())
	key = httpRequest.args["key"][0]
	with open("/home/tgard/.key","w") as f:
		f.write(key)

	httpResponse(response, httpRequest)


# get methods with path to method dictionary mapping
GET_METHODS = { HTTP_REQUEST_ALL_CONFIG: getAllCfg, 
				HTTP_REQUEST_ID: getGwyId,
				HTTP_REQUEST_TEMP_RECORDS: getDataRecords,
				HTTP_REQUEST_WIFI_SCAN: getNetWifiScan }


POST_METHODS = {HTTP_REQUEST_ID: postGwyId, 
				HTTP_REQUEST_SESSION_NEW: postTmpSeshNew,
				HTTP_REQUEST_SESSION_RESTART: postTmpSeshRestart,
				HTTP_REQUEST_SESSION_RESUME: postTmpSeshResume,			
				HTTP_REQUEST_USERS: postNfyCfg,
				HTTP_REQUEST_EMAIL_IMAGES: postEmailImages,
				HTTP_REQUEST_WIFI_CONNECT: postNetWifiConnect,
				HTTP_REQUEST_POWER_OFF: postSysPowerOff,
				HTTP_REQUEST_MUTE_ALARM: postAvMute,
				HTTP_REQUEST_AV_CONFIG: postAvConfig,
				HTTP_REQUEST_CREDENTIALS: postCredentials,
				HTTP_REQUEST_KEY: postKey }




def get(request):

	logging.debug('Shell: Render GET uri: {0}'.format(request.uri))
	logging.debug('Shell: Render GET args: {0}'.format(request.args))
	logging.debug('Shell: Render GET path: {0}'.format(request.path))

	if not request.path in GET_METHODS:
		logging.debug('Shell: Uri get not supported.')
		response = {'Result':1}
	else:
		# pass request to method and let method handle response
		response = GET_METHODS[request.path](request)

def post(request):

	logging.debug('Shell: Render POST __dict__: {0}'.format(request.__dict__))
	logging.debug('Shell: Render POST uri: {0}'.format(request.uri))
	logging.debug('Shell: Render POST args: {0}'.format(request.args))
	logging.debug('Shell: Render POST path: {0}'.format(request.path))
	logging.debug('Shell: Render POST content: {0}'.format(request.content.getvalue()))

	if not request.uri in POST_METHODS:
		logging.debug('Shell: Uri post not supported.')
		response = {'Result':1}
	else:
		response = POST_METHODS[request.uri](request)
		# push live state now
		reactLoopLiveState()





class SimpleShell(Resource): 
	''' @class SimpleShell
		@brief : Implements the GET methods. 
	'''
	isLeaf = True

	def render_GET(self, request):			
		thread = threading.Thread(target=get, args=(request,))
		thread.daemon = True
		thread.start()
		return NOT_DONE_YET
		
		
				


class Shell(Resource):
	''' @class : Shell
		@brief : Get/Post request handler.
	'''
	isLeaf = True

	def render_GET(self, request):
		thread = threading.Thread(target=get, args=(request,))
		thread.daemon = True
		thread.start()
		return NOT_DONE_YET

	def render_POST(self, request):
		thread = threading.Thread(target=post, args=(request,))
		thread.daemon = True
		thread.start()
		return NOT_DONE_YET


def reactLoopLiveState():
	''' @brief : Get live state of all interfaces '''
	global email, notification, audiovisual, event, system, temp, gateway, hamachi, simple, shell

	# build data 4th token should be network status ....mostly for lcd client
	livedata = '{0},{1},{2},{3},{4}'.format(system.getLiveState(), 
											hamachi.getLiveState(), 										
											audiovisual.getLiveState(),
											network.getLiveState(),
											temp.getLiveState())

	logging.debug('Shell: Updating live state.')
	# send event
	event.raiseEvent(tgEvent.EVCLASS_LIVE, tgEvent.EVTOPIC_LIVE_DATA, livedata, tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_STREAM])
	
def reactLoopDone(result):
	''' @brief : Reactor loop call failed. '''
	logging.info('Reactor loop: Complete.')


def reactLoopFailed(failure):
	''' @brief : Reactor loop call failed. '''
	logging.error('Reactor loop: Error = {}.'.format(failure.getBriefTraceback()))


def runApp(starttype=''):
	''' @brief : Initialise system '''

	global email, notification, audiovisual, event, system, temp, gateway, hamachi, simple, shell, network

	email = Email('', event)
	email.loadConfig()
	email.start()

	notification = Notification(email=email)
	notification.loadConfig()
	notification.start()

	audiovisual = AudioVisual()
	audiovisual.loadConfig()
	audiovisual.start()

	event = tgEvent.Event(reactor=reactor, notifications=notification, audvis=audiovisual, streamPort=STREAM_PORT)
	event.start()

	# power on event, no args sent when started by init
	if starttype == '':
		event.raiseEvent(tgEvent.EVCLASS_SYSTEM, tgEvent.EVTOPIC_SYS_POWERON, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE, tgEvent.EVACTION_NOTIFICATIONS])	

	temp = Temperature(event)
	temp.loadConfig()

	gateway = Gateway(event)
	gateway.loadConfig()
	email.setDeviceName(gateway.getConfig()['Id']['Alias'])

	network = Network(event)
	network.start()

	hamachi = Hamachi(event)
	hamachi.loadConfig()
	hamachi.start()

	system = System(event)
	system.start()

	simple = SimpleShell()
	factory = Site(simple)
	endpoint = TCP4ServerEndpoint(reactor, interface='0.0.0.0', port=SIMPLE_SHELL_PORT)
	endpoint.listen(factory)

	shell = Shell()
	factory = Site(shell)
	endpoint = TCP4ServerEndpoint(reactor, interface='0.0.0.0', port=SHELL_PORT)
	endpoint.listen(factory)

	# start temperature
	temp.start()

	# start live state timer
	l = task.LoopingCall(reactLoopLiveState)
	l.start(20)
	#l.addCallback(reactLoopDone)
	#l.addErrback(reactLoopFailed)

	logging.debug('Starting reactor.')
	# run reactor event loop
	reactor.run()






def exitApp(shutdownCause=SHUTDOWN_REASON_USER):
	''' @fn comCleanExit
		@brief : Clean exit handler when signal terminates program.
	'''	

	# stop services
	global email, notification, audiovisual, system, event, database, temp, gateway, hamachi, simple, shell, network

	audiovisual.stop()
	event.raiseEvent(tgEvent.EVCLASS_SYSTEM, tgEvent.EVTOPIC_SYS_SHUTDOWN, shutdownCause, tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE, tgEvent.EVACTION_NOTIFICATIONS])
	time.sleep(15)
	temp.stop()	
	hamachi.stop()
	network.stop()
	system.stop()	
	email.stop()
	notification.stop()
	event.stop()
	reactor.stop()
	logging.info('Tempgard: Reactor stopped.')
	logging.info('Tempgard: Exiting application.')


def fSignalHandler(signal, frame):		
	''' @fn fSignalHandler
		@brief : Signal handler.
	'''

	exitApp(SHUTDOWN_REASON_USER)


if __name__ == "__main__":

	# signal handlers
	signal.signal( signal.SIGINT, fSignalHandler )
	signal.signal( signal.SIGTERM, fSignalHandler )
	
	# root logger
	logger = logging.getLogger('')
	logger.setLevel(logging.DEBUG)
		
	# format for logging
	format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

	# output to file
	stdouth = logging.StreamHandler(sys.stdout)
	stdouth.setFormatter(format)
	logger.addHandler(stdouth)

	logging.info('tgserverd: Starting application.')

	runApp()