#!/usr/bin/env python2

''' @file : tgGateway.py
	@brief : Gateway.
'''


import logging
import json
import copy
import sys
import tggateway.tgEvent as tgEvent

GATEWAY_CONFIG_FILE 		= '/home/tgard/config/gateway.json'
GATEWAY_CONFIG_FILE_BAK 	= '/home/tgard/config/gateway.json.bak'

class Gateway(object):
	''' @class : Gateway
		@brief : Gateway.
	'''
	def __init__(self, eventMgr):
		''' @fn : __init__
			@brief : Class initialisation.
		'''
		self.eventMgr = eventMgr
		self.config = {}

		logging.info('Gateway: Initialised.')

	def loadConfig(self):
		'''
			@brief : Load config from file.
		'''
		try:
			with open(GATEWAY_CONFIG_FILE, 'r') as f:
				self.config = json.load(f)

			logging.debug('Gateway config:')
			logging.debug('	Identity Serial {0}.'.format(self.config['Id']['Serial']))
			logging.debug('	Identity Name {0}.'.format(self.config['Id']['Alias']))
			logging.debug('	Identity Descriptor 1 {0}.'.format(self.config['Id']['D1']))
			logging.debug('	Identity Descriptor 2 {0}.'.format(self.config['Id']['D2']))
			logging.debug('	Identity Descriptor 3 {0}.'.format(self.config['Id']['D3']))
			logging.debug('	Identity Descriptor 4 {0}.'.format(self.config['Id']['D4']))
			logging.debug('	Identity Descriptor 5 {0}.'.format(self.config['Id']['D5']))
		
			logging.debug('	Info Owner {0}.'.format(self.config['Info']['Owner']))
			logging.debug('	Info Contact {0}.'.format(self.config['Info']['Contact']))
			logging.debug('	Info Install date {0}.'.format(self.config['Info']['InstallDate']))
			logging.debug('	Version Lcd {0}.'.format(self.config['Version']['Lcd']))
			logging.debug('	Version Server {0}.'.format(self.config['Version']['Server']))
			logging.debug('	Version Hardware {0}.'.format(self.config['Version']['Hw']))
			
			#logging.debug('	Key {0}.'.format(self.config['Key']))

			logging.info('Gateway: Config loaded from file.')
			self.configured = True

		except Exception as e:
			logging.error("Gateway: Load config error {0}.".format(e))
			self.eventMgr.raiseEvent(tgEvent.EVCLASS_GATEWAY, tgEvent.EVTOPIC_GWY_NO_CONFIG, '', tgEvent.EVENT_PRIORITY_HIGH, [tgEvent.EVACTION_DATABASE])

	def dumpConfig(self):
		'''
			@brief : Dump config to file.
		'''
		try:
			with open(GATEWAY_CONFIG_FILE, 'w') as f:
				json.dump(self.config, f)
		
			logging.info('Gateway: Config dumped to file.')
		
		except Exception as e:
			logging.error("Gateway: Dump config error {0}.".format(e))

	def setConfig(self, config):
		''' 
			@brief : Set config.
		'''
		try:	
					
			# validate 
			if "Id" not in config:
				logging.warning("Gateway: Id not in config")
				return False, "Id not in config"

			if "Serial" not in config["Id"]:
				logging.warning("Gateway: Serial not in config")
				return False, "Serial not in config"

			if config["Id"]["Serial"] != self.config["Id"]["Serial"]:
				logging.warning("Gateway: Serial does not match")
				return False, "Serial does not match"

			if "Alias" not in config["Id"]:
				logging.warning("Gateway: Alais not in config")
				return False, "Alais not in config"

			self.config.clear()
			self.config = copy.deepcopy(config)
			self.dumpConfig()
			self.configured = True
			logging.debug('Gateway: Config set.')
			return True, "Success"
		except Exception as e:
			logging.error('Gateway: Config set error {}.'.format(e))
			# restore previous
			return False, e

	def setDefaultConfig(self):
		'''
			@brief : Set default config.
		'''
		self.config = {"Id":{"Serial":"tg987654321", "Alias":"", "D1":"", "D2":"", "D3":"", "D4":"", "D5":""},
						"Info":{"Owner":"", "Contact":"", "InstallDate":""},
						"Version":{"Lcd":"", "Server":"", "Hw":""}}
		self.dumpConfig()

	def getConfig(self):
		''' 
			@brief : Get config.
		'''
		copyCfg = copy.deepcopy(self.config)

		return copy.deepcopy(self.config)

	def getLiveState(self):
		''' @brief : getLiveState '''

		return

	def clone(self):
		'''
			@brief : Clone gateway.
		'''
		return

	def restore(self):
		'''
			@brief : Restore gateway.
		'''

	def factoryReset(self):
		'''
			@brief : Factory reset.
		'''
		try:
			logging.info('Gateway: Factory reset.')
			
			# restore factory config for gateway
			self.setDefaultConfig()
			
			return True
		except Exception as e:
			logging.error('Gateway: Unexpected error {0}.'.format(e))
			return False



def cleanExit():
	''' @fn cleanExit
		@brief : Clean exit handler when signal terminates program.
	'''	
	logging.info('Gateway: Exiting application.')
	sys.exit()

def fSignalHandler(signal, frame):		
	''' @fn fSignalHandler
		@brief : Signal handler.
	'''
	cleanExit()

if __name__ == '__main__':
	
	argc = len(sys.argv)	

	# root logger
	logger = logging.getLogger('')
	logger.setLevel(logging.DEBUG)
	
	# format for logging
	format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

	# add stdout stream handler
	stdouth = logging.StreamHandler(sys.stdout)
	stdouth.setFormatter(format)
	logger.addHandler(stdouth)

	tgEvt = tgEvent.Event()
	tgGwy = Gateway(tgEvt)
	tgGwy.loadConfig()