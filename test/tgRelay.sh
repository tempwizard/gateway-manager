#!/bin/sh

echo "Testing relay outputs"

printf "Testing K301 [EXT_ALARM1]"
echo 1 > /sys/class/gpio/gpio31_pb6/value
printf "."
sleep 1
printf "."
echo 0 > /sys/class/gpio/gpio31_pb6/value
echo " COMPLETE"

sleep 1

printf "Testing K302 [EXT_ALARM2]"
echo 1 > /sys/class/gpio/gpio32_pb7/value
printf "."
sleep 1
printf "."
echo 0 > /sys/class/gpio/gpio32_pb7/value
echo " COMPLETE"
