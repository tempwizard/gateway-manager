#!/bin/sh

echo "Testing internal buzzers"

printf "Testing BUZ301 [INT_ALARM]"
echo 1 > /sys/class/gpio/gpio30_pb5/value
printf "."
sleep 1
printf "."
echo 0 > /sys/class/gpio/gpio30_pb5/value
echo " COMPLETE"

sleep 1

printf "Testing BUZ302 [BEEP]"
echo 1 > /sys/class/gpio/gpio11_pe10/value
printf "."
sleep 1
printf "."
echo 0 > /sys/class/gpio/gpio11_pe10/value
echo " COMPLETE"