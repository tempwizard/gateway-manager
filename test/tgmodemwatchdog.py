#!/usr/bin/python2
#
# tgmodemwatchdogsimple.py
# Daniel Salamy 2015 (daniel@alfatron.com.au)

import dbus
import logging
import logging.handlers
import sys
import time
import os

def connmanCellularCheck():
    try:
        bus = dbus.SystemBus()
        manager = dbus.Interface(bus.get_object('net.connman', '/'), 'net.connman.Manager')
        services = manager.GetServices()
    except:
        logging.critical("dbus or connman threw an exception")
        return

    for service in services:
        logging.debug("Service found: %s", service[0])
        if "cellular" in service[0]:
            logging.debug("Cellular service found")
            return True
    return False

def connmanEthernetCheck():
    try:
        bus = dbus.SystemBus()
        manager = dbus.Interface(bus.get_object('net.connman', '/'), 'net.connman.Manager')
        services = manager.GetServices()
    except:
        logging.critical("dbus or connman threw an exception")
        return False

    for service in services:
        if any(s in service[0] for s in ["ethernet", "wifi"]):
            logging.debug("Ethernet or Wifi service found: %s", service[0])
            if any(s in service[1]["State"] for s in ["online", "ready"]):
                logging.debug("Online service found: %s", service[0])
                return True
                break
        else:
            return False

def ofonoRestart():
    try:
        bus = dbus.SystemBus()
        systemd = bus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
        manager = dbus.Interface(systemd, dbus_interface='org.freedesktop.systemd1.Manager')
    except:
        logging.critical("dbus or connman threw an exception")
        return

    ofono = manager.GetUnit("ofono.service")

    stop = manager.StopUnit("ofono.service","replace")
    logging.debug(stop)

    start = manager.StartUnit("ofono.service","replace")
    logging.debug(start)

    return

def gpioConfig(pin, pinLong):
    #Export GPIO
    if os.path.exists('/sys/class/gpio/' + pinLong + '/direction'):
        logging.debug("GPIO %s already configured", pinLong)
        return
    else:
        with open('/sys/class/gpio/export', 'w') as the_file:
            the_file.write(pin + '\n')
        #Set direction
        with open('/sys/class/gpio/' + pinLong + '/direction', 'w') as the_file:
            the_file.write('out\n')
        logging.debug("GPIO %s configured", pinLong)
        return

def gpioSetValue(pinLong, value):
    logging.debug("Setting GPIO %s to %s", pinLong, value)
    with open('/sys/class/gpio/' + pinLong + '/value', 'w') as the_file:
        the_file.write(value + '\n')
    return

def modemPowerCycle():
    gpioSetValue(gpioLong, "0")
    time.sleep(2)
    gpioSetValue(gpioLong, "1")
    return

def log_setup():
    #log_handler = logging.handlers.WatchedFileHandler('/var/log/tgard/tgmodemwatchdog.log')

    

    formatter = logging.Formatter(fmt='%(asctime)s [%(filename)s]%(levelname)8s %(message)s', datefmt='%b %d %H:%M:%S')
    #log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    #logger.addHandler(log_handler)
    stdouth = logging.StreamHandler(sys.stdout)
    stdouth.setFormatter(format)
    logger.addHandler(stdouth)
    logger.setLevel(logging.DEBUG)

if __name__ == "__main__":

    logger = logging.getLogger('')
      
    # format for logging
    format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

    stdouth = logging.StreamHandler(sys.stdout)
    stdouth.setFormatter(format)
    logger.addHandler(stdouth)
    logger.setLevel(logging.DEBUG)


    # logging.basicConfig(format='%(asctime)s [%(filename)s]%(levelname)8s %(message)s', filename="/var/log/tgard/tgmodemwatchdog.log", level=logging.INFO, datefmt="%b %d %H:%M:%S")

    logging.info("Starting tgmodemwatchdog.py")


    time.sleep(5)
    #Repeat frequency in seconds
    sleep = 60

    #GPIO pin connected to switch on power rail
    gpioPin = "13"
    gpioLong = "gpio13_pi14"

    # Setup gpio
    gpioConfig(gpioPin, gpioLong)
    # Set inital value
    # gpioSetValue(gpioLong, "1")
    modemPowerCycle()
    sys.exit()

    while True:
        '''if connmanEthernetCheck():
            logging.debug("Ethernet or WiFi connected, set 3G off")
            gpioSetValue(gpioLong, "0")
        else:'''
            #Check connman for modem service
        if connmanCellularCheck():
            logging.info("Connman has celluar service")
        else:
            logging.warning("Connman missing cellular service")
            logging.debug("Modem is being power cycled")
            modemPowerCycle()
            logging.debug("Ofono is being restarted")
            ofonoRestart()

        logging.debug("Check complete, sleeping for: %s seconds", sleep)
        time.sleep(sleep)

