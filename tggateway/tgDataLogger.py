#!/usr/bin/env python2

''' @file : tgDataLogger.py
	@brief : Logging of data and events.
'''

import os
import logging
from logging.handlers import TimedRotatingFileHandler as TRFHandler

TG_SESSION_DIR = "/home/tgard/session/{}"
TG_SESSION_DATA_LOG = "/home/tgard/session/{}/tdata.log"
TG_EVENT_DIR = "/home/tgard/event"
TG_EVENT_LOG = "/home/tgard/event/events.log"


class TnetEventLogger(object):
	''' @class:
		@brief: Events logger. Rotates weekly. 
	'''
	def __init__(self):
		self.logger = logging.getLogger('TnetEventLogger')
		self.logger.setLevel(logging.INFO)

		if not os.path.exists(TG_EVENT_DIR):
			logging.debug("Creating event directory")
			# create new dir 
			os.makedirs(TG_EVENT_DIR)

		# output to file
		handler = TRFHandler(TG_EVENT_LOG, when='W0', interval=1, backupCount=0, encoding=None, delay=True, utc=False)
		self.logger.addHandler(handler)

	def log(self, evDate, evClass, evTopic, evPriority, evData):
		self.logger.info("{}:{}:{}:{}:{}".format(evDate, evClass, evTopic, evPriority, evData))


class TnetDataLogger(object):
	''' @class:
		@brief: Temperature minute data logger. Rotates daily at midnight. 
	'''
	def __init__(self, batchSize=10):
		self.batchsize = batchSize
		self.batch = []
		self.logger = None

	def setBatchSize(self, batchsize):
		self.batchsize = batchsize
		logging.info("Set batch size to {}".format(self.batchsize))

	def log(self, evDate, evData):
		
		# flush batch
		if len(self.batch) == self.batchsize:
			
			for i in range(self.batchsize):
				self.logger.info(self.batch[i])

			del self.batch[:]

		newEntry = "{},{}".format(evDate, evData)
		self.batch.append(newEntry)

	def newSession(self, sessionName, removePrevious=True):
		''' @class: 
			@brief: Create new logging file by removing handler first and creating another.
		'''

		sessiondirname = TG_SESSION_DIR.format(sessionName)
		

		if not os.path.exists(sessiondirname):
			logging.info("Creating new session log directory {}".format(sessiondirname))
			# create new dir 
			os.makedirs(sessiondirname)

		# create logger
		self.logger = logging.getLogger("TnetDataLogger")
		self.logger.setLevel(logging.INFO)

		# remove previous log handler
		if removePrevious:
			try:
				self.logger.removeHandler("TnetDataLogger")
			except Exception as e:
				logging.error("Unable to remove previous logging handler e=".format(e))			

		logfilename = TG_SESSION_DATA_LOG.format(sessionName)
		# add new log handler
		handler = TRFHandler(logfilename, when='midnight', interval=1, backupCount=0, encoding=None, delay=True, utc=False)
		self.logger.addHandler(handler)

	def flushLogs(self):
		sizeofbatch = len(self.batch)
		for i in range(sizeofbatch):
			self.logger.info(self.batch[i])

		del self.batch[:]
		
		logging.info("Flushed logs")